<?php

namespace App\Http\Controllers;

use App\Models\Country;
use Illuminate\Http\Request;

class ValidateController extends Controller
{
    public function validate(Request $request, array $rules, array $messages = [], array $customAttributes = []){
        $request->validate([
            'answer' => 'required|exists:countries,id'
        ]);
        $user_answer = Country::find($request->answer);

        return ["correct" => $user_answer !== null && $user_answer->isCorrect()];
    }
}
