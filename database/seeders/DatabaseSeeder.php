<?php

namespace Database\Seeders;

use App\Models\Answer;
use App\Models\Country;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        Country::create([
            'name' => 'francia',
        ]);
        Country::create([
            'name' => 'germania',
        ]);
        Country::create([
            'name' => 'italia',
        ]);
        Answer::create([
            'country_id' => 3,
        ]);
    }
}
