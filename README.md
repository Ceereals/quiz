# Quiz

## Installation
    
```bash
composer install
```
```bash
php artisan key:generate
```

Controllare che il db "quiz" esista.

```bash
php artisan migrate:fresh --seed
```

```bash
npm install && npm run dev
```
