/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./resources/**/*.blade.php",
        "./resources/**/*.js",
        "./resources/**/*.vue",
    ],
    theme: {
        extend: {
            colors: {
                transparent: 'transparent',
                'white': '#ffffff',
                'green': "#0CC691",
                'green-light': "#85E2C8",
                'grey': "#BFC2CD",
                'grey-light': "#EFF0F2",
                'grey-dark': "#EAEBEF",
                'grey-darker': "#585e71",
                'red': "#ca2530",
            },
            fontfamily: {
                'inter': ["Inter", "sans-serif"],
            }
        },

    },
    plugins: [],
}
